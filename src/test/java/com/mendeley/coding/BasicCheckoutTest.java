package com.mendeley.coding;

import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertTrue;

public class BasicCheckoutTest {

    private Checkout checkout;

    @Before
    public void createCheckout(){
        checkout = new BasicCheckout();
    }

    @Test
    public void shouldReturnTotalPrice() {
        List<Item> items = new ListReader().read("src/test/resources/example-checkout.json");
        List<OfferType> discountsToApply = new ArrayList<>();
        String output = checkout.checkout(items, discountsToApply);
        assertTrue(output.contains("Total price : £31.00"));
        assertTrue(output.contains("Final price : £31.00"));
    }

    @Test
    public void shouldReturnTotalPriceAndAddFixedDiscount() {
        List<Item> items = new ListReader().read("src/test/resources/example-checkout.json");
        List<OfferType> discountsToApply = new ArrayList<>();
        discountsToApply.add(OfferType.FIXED);
        String output = checkout.checkout(items, discountsToApply);
        assertTrue(output.contains("Total price : £31.00"));
        assertTrue(output.contains("Final price : £30.00"));
    }

    @Test
    public void shouldReturnTotalPriceAndAddPairProductsDiscount() {
        List<Item> items = new ListReader().read("src/test/resources/example-checkout.json");
        List<OfferType> discountsToApply = new ArrayList<>();
        discountsToApply.add(OfferType.IDENTICAL_PAIR);
        String output = checkout.checkout(items, discountsToApply);
        assertTrue(output.contains("Total price : £31.00"));
        assertTrue(output.contains("Final price : £28.00"));
    }

    @Test
    public void shouldReturnTotalPriceAndAddCheapestProductDiscount() {
        List<Item> items = new ListReader().read("src/test/resources/example-checkout.json");
        List<OfferType> discountsToApply = new ArrayList<>();
        discountsToApply.add(OfferType.MULTIPLE_ITEMS);
        String output = checkout.checkout(items, discountsToApply);
        assertTrue(output.contains("Total price : £31.00"));
        assertTrue(output.contains("Final price : £26.00"));
    }

    @Test
    public void shouldReturnTotalPriceAndAddAllDiscounts() {
        List<Item> items = new ListReader().read("src/test/resources/example-checkout.json");
        List<OfferType> discountsToApply = new ArrayList<>();
        discountsToApply.add(OfferType.MULTIPLE_ITEMS);
        discountsToApply.add(OfferType.IDENTICAL_PAIR);
        discountsToApply.add(OfferType.FIXED);
        String output = checkout.checkout(items, discountsToApply);
        assertTrue(output.contains("Total price : £31.00"));
        assertTrue(output.contains("Final price : £22.00"));
    }

}
