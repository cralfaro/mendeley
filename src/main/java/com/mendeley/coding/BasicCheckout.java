package com.mendeley.coding;

import java.math.BigDecimal;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class BasicCheckout implements Checkout {

    private static final Logger LOGGER = Logger.getLogger(BasicCheckout.class.getName());

    @Override
    public String checkout(List<Item> items, List<OfferType> offerTypes) {
        StringBuilder stringBuilder = new StringBuilder();

        items.forEach(item -> stringBuilder
                .append(item.getName())
                .append(" : £")
                .append(item.getPrice())
                .append("\n"));

        BigDecimal sum = items.stream().map(Item::getPrice).reduce(BigDecimal.ZERO, BigDecimal::add);
        stringBuilder.append("Total price : £").append(sum);

        BigDecimal fixedDiscounts = new BigDecimal(0);
        BigDecimal pairDiscounts = new BigDecimal(0);
        BigDecimal cheapestFreeDiscounts = new BigDecimal(0);
        if(offerTypes.contains(OfferType.FIXED)) {
            fixedDiscounts = applyFixedDiscounts(items);
            sum = sum.subtract(fixedDiscounts);
        }
        if(offerTypes.contains(OfferType.IDENTICAL_PAIR)) {
            pairDiscounts = applyPairDiscount(items);
            sum = sum.subtract(pairDiscounts);
        }
        if(offerTypes.contains(OfferType.MULTIPLE_ITEMS)) {
            cheapestFreeDiscounts = applyCheapestFreeDiscount(items);
            sum = sum.subtract(cheapestFreeDiscounts);
        }
        stringBuilder.append(" Final price : £").append(sum);
        StringBuilder discountsDetails = new StringBuilder();
        final StringBuilder discountDetails = discountsDetails.append("\n\n *********** \n Discounts:")
                .append("\n").append("   Fixed: £").append(fixedDiscounts)
                .append("\n").append("   Pair: £" + pairDiscounts).append("\n")
                .append("   Free Cheapest: £" + cheapestFreeDiscounts).append("\n");

        String output = stringBuilder.toString();
        output += discountDetails.toString();
        LOGGER.log(Level.INFO, output);
        return output;
    }

    private BigDecimal applyCheapestFreeDiscount(List<Item> items) {
        BigDecimal cheapestFreeDiscounts = new BigDecimal(0);
        Map<String, List<Item>> itemsByType = new HashMap<>();
        items.forEach(item -> {
            List<Item> itemsType = itemsByType.get(item.getType());
            if (itemsType == null) {
                List<Item> pairList = new ArrayList<>();
                pairList.add(item);
                itemsByType.put(item.getType(), pairList);
            } else {
                itemsType.add(item);
                itemsByType.put(item.getType(), itemsType);
            }
        });
        for(String keyName : itemsByType.keySet()){
            if(itemsByType.get(keyName).size() > 2){
                int threeTuple = itemsByType.get(keyName).size() / 3;
                List<Item> itemsBySpecificType = itemsByType.get(keyName);
                Collections.sort(itemsBySpecificType);
                for(int i = 0; i < threeTuple; i++){
                    cheapestFreeDiscounts = cheapestFreeDiscounts.add(itemsBySpecificType.get(i).getPrice());
                }
            }
        }
        return cheapestFreeDiscounts;
    }


    private BigDecimal applyPairDiscount(List<Item> items) {
        BigDecimal discount = new BigDecimal(0);
        Map<String, List<Item>> itemsByName = new HashMap<>();
        items.stream().filter(item -> item.getPromotionalPrice() != null).forEach(item -> {
            List<Item> itemsName = itemsByName.get(item.getName());
            if(itemsName == null) {
                List<Item> pairList = new ArrayList<>();
                pairList.add(item);
                itemsByName.put(item.getName(), pairList);
            } else {
                itemsName.add(item);
                itemsByName.put(item.getName(), itemsName);
            }
        });
        for(String keyName : itemsByName.keySet()){
            if(itemsByName.get(keyName).size() > 1){
                int pairsNumber = itemsByName.get(keyName).size() / 2;
                Item sampleItem = itemsByName.get(keyName).get(0);
                BigDecimal pairPrice = sampleItem.getPrice().multiply(new BigDecimal(2));
                pairPrice = pairPrice.subtract(sampleItem.getPromotionalPrice());
                discount = discount.add(new BigDecimal(pairsNumber).multiply((pairPrice)));
            }
        }
        return discount;
    }

    private BigDecimal applyFixedDiscounts(List<Item> items) {
        BigDecimal fixedDiscounts = new BigDecimal(0);
        for(Item item: items){
            if(item.getFixedDiscount().doubleValue() > 0.0){
                fixedDiscounts = fixedDiscounts.add(item.getPrice().multiply(item.getFixedDiscount()));
            }
        }
        return fixedDiscounts;
    }
}
