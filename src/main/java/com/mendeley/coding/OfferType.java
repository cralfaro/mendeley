package com.mendeley.coding;

/**
 * Created by ruben.alfarodiaz on 22/09/2017.
 */
public enum OfferType {
    FIXED,
    MULTIPLE_ITEMS,
    IDENTICAL_PAIR;
}
