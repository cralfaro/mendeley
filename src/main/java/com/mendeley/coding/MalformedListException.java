package com.mendeley.coding;

public class MalformedListException extends RuntimeException {
    public MalformedListException(Exception e) {
        super(e);
    }
}
