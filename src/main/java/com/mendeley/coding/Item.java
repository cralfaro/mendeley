package com.mendeley.coding;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

public class Item implements Comparable<Item>{
    private String name;
    private String type;
    private BigDecimal price;
    private BigDecimal fixedDiscount;
    private BigDecimal promotionalPrice;


    @JsonCreator
    public Item(@JsonProperty("name") String name,
                @JsonProperty("type") String type,
                @JsonProperty("price") BigDecimal price,
                @JsonProperty("fixedDiscount") BigDecimal fixedDiscount,
                @JsonProperty("promotionalPrice") BigDecimal promotionalPrice) {
        this.name = name;
        this.price = price;
        this.type = type;
        this.fixedDiscount = fixedDiscount;
        this.promotionalPrice = promotionalPrice;

    }

    String getName() {
        return name;
    }

    BigDecimal getPrice() {
        return price;
    }

    String getType() {
        return type;
    }

    BigDecimal getFixedDiscount() {
        return fixedDiscount;
    }

    BigDecimal getPromotionalPrice() {
        return promotionalPrice;
    }

    @Override
    public int compareTo(Item o) {
        return this.getPrice().compareTo(o.getPrice());
    }
}
