package com.mendeley.coding;

import java.util.ArrayList;
import java.util.List;

public class CheckoutApplication {

    public static void main(String[] args) {

        if (args.length < 1) {
            throw new RuntimeException("No path to the input list was provided.");
        }

        String filePath = args[0];
        List<OfferType> offerTypes = new ArrayList<>();
        for(int i = 1; i < args.length; i++){
            offerTypes.add(OfferType.valueOf(args[i]));
        }

        List<Item> itemList = new ListReader().read(filePath);

        Checkout basicCheckout = new BasicCheckout();

        String output = basicCheckout.checkout(itemList, offerTypes);
    }
}
