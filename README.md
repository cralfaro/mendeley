# Mendeley Coding Exercise

Please write a console application that simulates a simple supermarket checkout. It needs to meet the following requirements:

* The application should read a list of items and prices from a JSON file. Please include an example input file.

* From the list of items and prices, the application should calculate the total price, including any discounts.

* Discounts are calculated based on rules defined in the system, and should be applied automatically. The following types of discount should be calculated:

* A fixed percentage off an item (e.g. 50% off)

* Buy 3 items of a type, get the cheapest free (e.g. buy three boxes of cereal, get the cheapest free)

* Buy 2 identical items receive a promotional price(e.g. buy 2 for £2)

* The application should output the total price of the items, a list of the items bought and any special offers applied

* It should be easy to add more special offer rules

* The application should be appropriately tested

* A readme file should be included to explain project usage, and how to add more special offers.

We expect you to take 2 to 4 hours to complete this exercise. Please prioritise producing a running console application over implementing all the requirements.

There is some code provided to you already to start you off - feel free to change/adapt anything you deem necessary.

Please provide your completed code as a zip file to your contact.

# Ruben Alfaro:

* Example file is located in /src/test/resources/example-checkout.json
* To run the app you need to use the args parameters and add all offers you want to apply
 For example src/test/resources/example-checkout.json FIXED
 All possible offers to apply are in OfferType enum
* For adding a new offer type, we should create the new enum value and add to the BasicCheckOut.checkout method the filter
 to apply the new discounts